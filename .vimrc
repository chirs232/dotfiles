imap jj <Esc>
imap JJ <Esc>

syntax on
set nu rnu

call plug#begin('~/.vim/plugged')

Plug 'morhetz/gruvbox'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

call plug#end()

set t_Co=256
let g:airline_theme='gruvbox'
colorscheme gruvbox
set bg=dark

" this is slowing down the pc
" autocmd TextChanged,TextChangedI <buffer> silent write

" Uncomment the following to have Vim jump to the last position when
" reopening a file
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

